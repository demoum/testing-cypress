/// <reference types="cypress" />

describe("Tickets", () => {
  beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

  //Digitando um campo de texto//
  it("Preencha todos os campos de entrada de texto.", () => {
    const firstName = "Matheus";
    const lastName = "Carneiro de Moura";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("matheus.moura@gmail.com");
    cy.get("#requests").type("Vegetariano");
    cy.get("#signature").type(`${firstName} ${lastName}`);
  });

  //Interagindo com elementos do tipo select//
  it("Selecione os tickets", () => {
    cy.get("#ticket-quantity").select("2");
  });

  //Interagindo com raddio buttons//
  it("Selecione 'vip' o ticket vip", () => {
    cy.get("#vip").check();
  });

  it("Selecione 'social media' checkbox", () => {
    cy.get("#social-media").check();
  });

  //Interagindo com checkboxes//
  it("Selecione 'friend' e 'publicação', então desmarque 'friend'", () => {
    cy.get("#friend").check();
    cy.get("#publication").check();
    cy.get("#friend").uncheck();
  });

  it("Verificar se tem 'TICKETBOX' no cabeçalho", () =>{
    cy.get("header h1").should("contain", "TICKETBOX");
  })

  // Verificar se e e-mail é válido //
  it.only("alerta de e-mail invalido", () => {
    cy.get("#email")
      .as("email")
      .type("matheus.moura-gmail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email")
      .clear()
      .type("matheus2021.moura@gmail.com")

    cy.get("#email-invalid").should("not.exist");
  });

});
