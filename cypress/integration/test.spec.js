/// <reference types="cypress" />

describe("Tickets", () => {
  beforeEach(() =>
    cy.visit(
      "https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"
    )
  );

  it("Preenche e reinicia o formulário", () => {
    const firstName = "Matheus";
    const lastName = "Moura";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("matheus.moura@gmail.com");
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#friend").check();
    cy.get("#requests").type("Ingressos cinema!");

    cy.get(".agreement p").should(
      "contain",
      `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get("#agree").click();
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("button[type='reset']").click();

    cy.get("@submitButton").should("be.disabled");
  });

  it("Preenche campos obrigatórios usando o comando de suporte", () => {
    const customer = {
      firstName: "João",
      lastName: "Silva",
      email: "joaosilva@example",
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
      .as("submitButton")
      .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");
  });
});
